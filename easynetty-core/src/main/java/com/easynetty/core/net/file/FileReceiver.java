package com.easynetty.core.net.file;

import com.easynetty.core.net.entity.EData;
import com.easynetty.core.net.entity.FileInfo;
import com.easynetty.core.net.entity.FileStatus;
import com.easynetty.core.utils.JsonUtil;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

;

/**
 * Created by Mengyue on 2016-07-17.
 */
public class FileReceiver {
    public static void receive(EData data, String dic, FinishCallBack cb) throws IOException {
        FileInfo fileInfo = JsonUtil.mapper().convertValue(data.getParams(), FileInfo.class);
        File f = new File(dic + fileInfo.getFileName());
        if (FileStatus.create.equals(fileInfo.getFileStatus())) {
            FileUtils.deleteQuietly(f);
        } else if (FileStatus.append.equals(fileInfo.getFileStatus())) {
            FileUtils.writeByteArrayToFile(f, data.getBody(), true);
        } else if (FileStatus.finish.equals(fileInfo.getFileStatus())) {
            cb.accept(fileInfo);
        }
    }
}
